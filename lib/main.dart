import 'package:flutter/material.dart';
import 'package:note/presentation/ui/create_update_screen.dart';
import 'package:note/presentation/ui/home_screen.dart';
import 'package:note/presentation/ui/not_found_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Note App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.lightGreen),
        useMaterial3: true,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => const HomeScreen(),
        '/createUpdateScreen': (context) => const CreateUpdateScreen(),
      },
      onGenerateRoute: (settings) {
        if (settings.name == '/') {
          return MaterialPageRoute(
            builder: (context) => const HomeScreen(),
            settings: settings,
          );
        }
        if (settings.name == '/createUpdateScreen') {
          return MaterialPageRoute(
            builder: (context) => CreateUpdateScreen(),
            settings: settings,
          );
        }

        return MaterialPageRoute(
          builder: (context) => NotFoundScreen(),
          settings: settings,
        );
      },
    );
  }
}
