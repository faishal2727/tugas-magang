import 'dart:ui';

class Note {
  final String id;
  final String title;
  final String description;
  final Color color;

  Note({
    required this.id,
    required this.title,
    required this.description,
    required this.color,
  });
}
