import 'package:flutter/material.dart';

class BottomSheetCreateUpdate extends StatelessWidget {
  final Function(Color) onSelectColor;

  BottomSheetCreateUpdate({required this.onSelectColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: Column(
        children: [
          Center(
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
              margin: const EdgeInsets.all(10),
              height: 5,
              width: 30,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          ListTile(
            leading: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(40),
                ),
                color: Color.fromARGB(255, 129, 170, 240),
              ),
              height: 40,
              width: 40,
            ),
            title: const Text('Blue'),
            onTap: () {
              onSelectColor(const Color.fromARGB(255, 129, 170, 240));
              Navigator.pop(context);
            },
          ),
          // ... other color options
        ],
      ),
    );
  }
}
