import 'package:flutter/material.dart';

class NoteTextFields extends StatelessWidget {
  final TextEditingController titleController;
  final TextEditingController descriptionController;

  NoteTextFields({required this.titleController, required this.descriptionController});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: titleController,
          decoration: const InputDecoration(
            hintText: "Title",
            border: InputBorder.none,
          ),
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        TextField(
          controller: descriptionController,
          maxLines: null,
          decoration: const InputDecoration(
              hintText: "Write something here...",
              border: InputBorder.none),
          style: TextStyle(fontSize: 12),
        ),
      ],
    );
  }
}
