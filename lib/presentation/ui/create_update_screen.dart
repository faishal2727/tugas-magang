import 'package:flutter/material.dart';
import 'package:note/model/note.dart';
import 'package:note/presentation/component/bottom_sheet_create_update.dart';
import '../component/note_text_fields.dart';

class CreateUpdateScreen extends StatefulWidget {
  final Note? editNote;
  const CreateUpdateScreen({Key? key, this.editNote}) : super(key: key);

  @override
  State<CreateUpdateScreen> createState() => _CreateUpdateScreenState();
}

class _CreateUpdateScreenState extends State<CreateUpdateScreen> {
  Color _selectedColor = Colors.white;
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.editNote != null) {
      // Pengecekan Jika ada data dari home screen, isi field dengan data dari note tersebut
      _titleController.text = widget.editNote!.title;
      _descriptionController.text = widget.editNote!.description;
      _selectedColor = widget.editNote!.color;
    }
  }

  // Function untuk update note
  void _updateNote() {
    final updatedNote = Note(
      id: widget.editNote!.id, // Tetap gunakan id dari note yang akan diedit
      title: _titleController.text,
      description: _descriptionController.text,
      color: _selectedColor,
    );

    // Setelah update berhasil akan pindah ke home screen
    Navigator.pop(context, updatedNote);
  }

  // Function untuk save note
  void _saveNote() {
    final newNote = Note(
      id: _generateUniqueId(),
      title: _titleController.text,
      description: _descriptionController.text,
      color: _selectedColor,
    );

    // Setelah save berhasil akan pindah ke home screen
    Navigator.pop(context, newNote);
  }

  // Function untuk generate Id
  String _generateUniqueId() {
    return DateTime.now().millisecondsSinceEpoch.toString();
  }

  // Function untuk ganti warna
  void _changeColor(Color newColor) {
    setState(() {
      _selectedColor = newColor;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Create Note"),
        backgroundColor: Colors.lightGreen,
      ),
      body: Container(
        color: _selectedColor,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: NoteTextFields(
            titleController: _titleController,
            descriptionController: _descriptionController,
          ),
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Transform.scale(
            scale: 0.7,
            child: FloatingActionButton(
              heroTag: null,
              onPressed: () {
                showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return BottomSheetCreateUpdate(
                      onSelectColor: _changeColor,
                    );
                  },
                );
              },
              child: const Icon(Icons.color_lens),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          FloatingActionButton(
            heroTag: null,
            onPressed: () {
              if (_titleController.text.trim().isEmpty ||
                  _descriptionController.text.trim().isEmpty) {
                // Menampilkan alert jika title atau deskripsi kosong
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text("Warning"),
                      content: const Text("Please fill title and description."),
                      actions: <Widget>[
                        TextButton(
                          child: Text("OK"),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              } else {
                if (widget.editNote != null) {
                  _updateNote(); // Panggil function edit
                } else {
                  _saveNote(); // Panggil function create
                }
              }
            },
            child: const Icon(Icons.save),
          ),
        ],
      ),
    );
  }
}
