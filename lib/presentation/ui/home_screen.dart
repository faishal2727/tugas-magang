import 'package:flutter/material.dart';
import 'package:note/presentation/component/bottom_sheet_home.dart';
import 'package:note/presentation/component/card_note.dart';
import '../../model/note.dart';
import 'create_update_screen.dart';
import 'detail_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Note> _notes = [];
  bool _isGridView = false;

  void _editNote(Note note) async {
    final updatedNote = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CreateUpdateScreen(editNote: note),
      ),
    ) as Note?;

    if (updatedNote != null) {
      setState(() {
        final index =
            _notes.indexWhere((element) => element.id == updatedNote.id);
        if (index != -1) {
          _notes[index] = updatedNote;
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Notes"),
        backgroundColor: Colors.lightGreen,
        actions: _notes.isNotEmpty
            ? [
                IconButton(
                  icon: _isGridView
                      ? const Icon(Icons.view_list)
                      : const Icon(Icons.grid_view),
                  onPressed: () {
                    setState(() {
                      _isGridView = !_isGridView;
                    });
                  },
                ),
              ]
            : [],
      ),
      body: _notes.isEmpty
          ? const Padding(
              padding: EdgeInsets.all(8.0),
              child: Center(
                child: Text("Make your first note"),
              ),
            )
          : _isGridView
              ? GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  itemCount: _notes.length,
                  itemBuilder: (context, index) {
                    final note = _notes[index];
                    return CardNote(
                      note: note,
                      onTap: _navigateToDetail,
                      onLongPress: _showBottomSheet,
                    );
                  },
                )
              : ListView.builder(
                  itemCount: _notes.length,
                  itemBuilder: (context, index) {
                    final note = _notes[index];
                    return CardNote(
                      note: note,
                      onTap: _navigateToDetail,
                      onLongPress: _showBottomSheet,
                    );
                  },
                ),
      floatingActionButton: FloatingActionButton(
        onPressed: _navigateToCreateUpdateScreen,
        child: const Icon(Icons.add),
      ),
    );
  }

  void _navigateToDetail(Note note) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailScreen(note: note),
      ),
    );
  }

  void _showBottomSheet(Note note) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return BottomSheetHome(
          note: note,
          onEdit: _editNote,
          onDelete: _deleteNote,
        );
      },
    );
  }

  void _deleteNote(Note note) {
    setState(() {
      _notes.remove(note);
    });
    Navigator.pop(context);
  }

  void _navigateToCreateUpdateScreen() async {
    final newNote =
        await Navigator.pushNamed(context, '/createUpdateScreen') as Note;
    if (newNote != null) {
      setState(() {
        _notes.add(newNote);
      });
    }
  }
}
